from ui import MainWindow


def main():
    w = MainWindow()
    w.run()


if __name__ == '__main__':
    main()
