from datetime import datetime
from operator import sub
from collections import defaultdict
from decimal import Decimal

import tkinter as tk

TIME_FORMAT = '%H:%M'


class MainWindow(tk.Tk):
    DEFAULT_WIDTH = 300
    DEFAULT_HEIGHT = 450
    PROPORTION_KEY = 'орг: пр'

    run = tk.Tk.mainloop

    def __init__(self):
        super().__init__()
        self.geometry('{}x{}'.format(self.DEFAULT_WIDTH, self.DEFAULT_HEIGHT))

        self._create_tool_pane()

        self.intervals_count = 0
        self.intervals = []

    def _create_tool_pane(self):
        self.tool_pane = ToolPane(self)
        self.tool_pane.grid(row=0, column=0, sticky='w')

        self.tool_pane.add_btn['command'] = self.add_interval
        self.tool_pane.export_btn['command'] = self.export_info

    def add_interval(self):
        interval_pane = IntervalPane(self)

        current_time = datetime.now().strftime(TIME_FORMAT)
        interval_pane.start_time.var.set(current_time)
        if self.intervals_count > 0 and not self.intervals[-1].finish_time.var.get():
            self.intervals[-1].finish_time.var.set(current_time)
        interval_pane.label.var.set(self.PROPORTION_KEY)

        self.intervals.append(interval_pane)
        self.intervals_count += 1
        interval_pane.grid(row=self.intervals_count, column=0, sticky='w')

    def update_info(self):
        def to_num(x, type_):
            try:
                return type_(x)
            except:
                return type_()
        minutes = sum(to_num(e.minutes['text'], int) for e in self.intervals)
        hours = sum(to_num(e.hours['text'], Decimal) for e in self.intervals)
        self.tool_pane.info_label['text'] = '{} min, {:.2f} hours'.format(minutes, hours)

    def export_info(self):
        current_time = datetime.now().strftime(TIME_FORMAT)
        if (self.intervals_count > 0
                and not self.intervals[-1].finish_time.var.get()):
            self.intervals[-1].finish_time.var.set(current_time)
        data = defaultdict(Decimal)
        for interval in self.intervals:
            key = interval.label.var.get()
            data[key] += Decimal(interval.hours['text'])

        if (self.PROPORTION_KEY in data
                and data[self.PROPORTION_KEY]
                and len(data) > 1):
            proportional = data.pop(self.PROPORTION_KEY)
            total = sum(data.values())
            for key, value in data.items():
                data[key] = value + value*proportional/total

        filename = 'accounting_{}.txt'.format(
            datetime.now().strftime('%Y%m%d_%H%M%S'))
        with open(filename, 'w', encoding='utf-8') as file:
            # file.write(json.dumps(txt_data, indent=4))
            for k, v in data.items():
                file.write('{}: {:.2f}\n'.format(k, v))


class ToolPane(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        self.widgets = []

        self.add_btn = tk.Button(self, text='Add')
        self.export_btn = tk.Button(self, text='Export')
        self.info_label = tk.Label(self, text='some info')

        self.widgets.extend((
            self.add_btn,
            self.export_btn,
            self.info_label
        ))
        for i, widget in enumerate(self.widgets):
            widget.grid(row=0, column=i)


class IntervalPane(tk.Frame):
    def __init__(self, parent):
        super().__init__(parent)

        entry_params = (
            dict(name='start_time', width=5, trace=True),
            dict(name='label', width=10),
            dict(name='finish_time', width=5, trace=True),
        )

        for i, params in enumerate(entry_params):
            entry = VarEntry(self, width=params['width'])
            setattr(self, params['name'], entry)
            entry.grid(row=0, column=i, sticky='w')
            if params.get('trace', False):
                entry.var.trace('w', lambda name, index, mode,
                                           var=entry.var: self._callback())

        label_params = (
            'minutes',
            'hours',
        )

        for i, name in enumerate(label_params, len(entry_params)):
            label = tk.Label(self)
            setattr(self, name, label)
            label.grid(row=0, column=i, sticky='w')

    def _callback(self):
        finish = self.finish_time.var.get()
        if not finish:
            return
        delta = int(self._get_delta())
        # TODO: Исправить на нормальное вычисление
        # minutes = delta
        # hours = delta / 60
        minutes = delta // 60
        hours = delta / 3600
        self.minutes['text'] = '{}'.format(minutes)
        self.hours['text'] = '{:.2f}'.format(hours)
        self.master.update_info()

    def _get_delta(self):
        names = ('finish_time', 'start_time')
        try:
            return sub(*(
                datetime.strptime(getattr(self, name).var.get(), TIME_FORMAT)
                for name in names
            )).total_seconds()
        except ValueError:
            return 0.0


class VarEntry(tk.Entry):
    def __init__(self, parent, *args, **kwargs):
        if 'textvariable' not in kwargs:
            self.var = tk.StringVar()
            kwargs['textvariable'] = self.var
        else:
            self.var = kwargs['textvariable']
        super().__init__(parent, *args, **kwargs)
